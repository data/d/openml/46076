# OpenML dataset: AIDS_Virus_Infection_Prediction

https://www.openml.org/d/46076

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The AIDS_Classification_50000.csv dataset is a comprehensive resource specifically compiled for researchers and healthcare professionals focusing on the statistical analysis of AIDS (Acquired Immunodeficiency Syndrome). Composed of 50,000 instances, this dataset encapsulates a broad spectrum of clinical and demographic variables related to AIDS patients. Each record in the dataset holds data across 23 columns, indicating various patient attributes including treatment details, demographic information, clinical test results, and disease progression indicators.

Attribute Description:
- `time`: Time since the baseline measurement, in days.
- `trt`: Treatment code (0, 1, 2), where each number signifies a different treatment regimen.
- `age`: Age of the patient in years.
- `wtkg`: Weight of the patient in kilograms.
- `hemo`: Presence of Hemophilia (0 = No, 1 = Yes).
- `homo`: Homosexual behavior (0 = No, 1 = Yes).
- `drugs`: Drug use (0 = No, 1 = Yes).
- `karnof`: Karnofsky score indicating patient's functional impairment (scores range from 0 to 100).
- `oprior`: Number of opportunistic infections prior to study.
- `z30`: Presence of Z30 gene (0 = No, 1 = Yes).
- `preanti`: Months before receiving antiretroviral therapy.
- `race`: Race (0 = Non-white, 1 = White).
- `gender`: Gender (0 = Female, 1 = Male).
- `str2`: Stratification variable 2.
- `strat`: Overall stratification.
- `symptom`: Presence of specific AIDS-related symptoms (0 = No, 1 = Yes).
- `treat`: Treatment response (0 = No, 1 = Yes).
- `offtrt`: Off treatment (0 = No, 1 = Yes).
- `cd40`: CD4 count at the baseline.
- `cd420`: CD4 count at 20 weeks.
- `cd80`: CD4 count at 8 weeks.
- `cd820`: CD4 count at 20 weeks post the 8-week measurement.
- `infected`: HIV infection status (0 = Negative, 1 = Positive).

Use Case:
This dataset is designed to facilitate a range of scientific inquiries, including the evaluation of treatment efficacy, the identification of prognostic factors for disease progression, and the development of predictive models for patient outcomes. By encompassing a rich variety of data points, the AIDS_Classification_50000.csv supports detailed statistical analyses and machine learning efforts aimed at enhancing our understanding of AIDS. It can particularly serve in the optimization of treatment protocols and in the advancement of targeted therapies, ultimately contributing to improved patient care and management strategies in the field of AIDS research.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46076) of an [OpenML dataset](https://www.openml.org/d/46076). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46076/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46076/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46076/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

